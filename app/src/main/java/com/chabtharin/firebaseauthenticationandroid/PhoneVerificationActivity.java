package com.chabtharin.firebaseauthenticationandroid;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.chabtharin.firebaseauthenticationandroid.databinding.ActivityPhoneVerificationBinding;

public class PhoneVerificationActivity extends AppCompatActivity {
    private ActivityPhoneVerificationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPhoneVerificationBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        PhoneActivity phoneActivity = new PhoneActivity();
        binding.mbVerify.setOnClickListener(view1 -> {
            phoneActivity.verifyPhoneNumberWithCode(binding.edtCode.getText().toString());
        });
    }
}